### Customer Requirement
`I would like to visualise the first 30 contributors of any GitHub project. I would like to see the results visualised as a graph, displaying the number of contributions per user.`

### Current State
This project provides the barebones to meet the above requirements.

The app is built with AngularJS and is bundled with Webpack to output a single file. It utilises Gitlab CI to automatically publish any pushes to Gitlab Pages (if using Gitlab as your own repository).

### Task
The project is currently lacking in many areas in terms of usability.

It is your task to improve the project:
- User experience
- Result visualisation (D3js has been included in the bundle, and is available from the D3 factory)
- Testing (Karma has been set up, but no tests written, and can be started with `npm run test` or `npm run test-watch`)
- Styling

Feel free to edit anything; wording, text, comments, implementations, folder hierarchy, npm packages, etc...

An example of a project to search for in the application would be electron/electron (https://github.com/electron/electron).

### Prerequisites

- Git https://git-scm.com/downloads
- Node https://nodejs.org/en/
- Google Chrome (for testing) https://www.google.com/chrome/index.html

### Project Setup

- Clone the branch
- Branch `git checkout -b my-branch-name`
- Install dependencies `npm i`
- Start up dev server `npm run dev`
- Run tests (and watch for changes) `npm run test-watch`

### Submission

Your project is unique to you and have been provided with a unique code. Please only push/pull that project.

Create a merge (pull) request for your branch into master of the given project so that we can see your progress.
