import '../app.js'
import Rx from 'rxjs/Rx';

const inject = angular.mock.inject;
const angModule = angular.mock.module;

describe('service: GitHub', function() {
    let GitHub, httpMock;

    beforeEach(angModule('githubApp'));

    beforeEach(() => {
        httpMock = {
            get: () => Promise.resolve()
        };

        angModule($provide => {
            $provide.factory('$http', () => httpMock);
        });

        inject($injector => {
            GitHub = $injector.get('GitHub');
        });
    });

    it('', function() {

    });
});