const API_URL = 'https://api.github.com/repos';

const githubService = function($http, Rx) {
    // http://reactivex.io/rxjs/manual/overview.html#subject
    this.contributors = new Rx.Subject();

    // Call the GitHub api and wrap the response into an Observable
    this.getContributors = (user, repo) => {
        const request = $http.get(`${API_URL}/${user}/${repo}/contributors`);

        // http://reactivex.io/rxjs/manual/overview.html#observable
        new Rx.Observable
            .fromPromise(request)
            .first()
            .subscribe(d => this.contributors.next(d))
        ;
    }
};

export default githubService;