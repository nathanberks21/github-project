import './styles/main.less';

// Components
import lookup from './components/lookup';
import output from './components/output';

// Factories
import rxFactory from './factories/rx.factory';
import d3Factory from './factories/d3.factory';

// Services
import githubService from './services/github.service';

angular
    .module('githubApp', [lookup, output])
    .factory('Rx', rxFactory)
    .factory('D3', d3Factory)
    .service('GitHub', githubService)
;