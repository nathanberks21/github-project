import output from './output.component';

export default angular
    .module('github.output', [])
    
    .component('output', output)

    .name
;
