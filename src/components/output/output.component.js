import template from './output.template.html';

const output = {
    template,
    controller: function(GitHub) {
        // http://reactivex.io/rxjs/manual/overview.html#subscribing-to-observables
        const sub = GitHub.contributors
            .subscribe(
                contributors => {
                    this.output = contributors;
                    console.log('Data received:', contributors);
                },
                err => console.error('An error occurred:', err)
            )
        ;

        this.$onDestroy = () => sub.unsubscribe();
    }
};

export default output;