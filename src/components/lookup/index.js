import lookup from './lookup.component';

export default angular
    .module('github.lookup', [])
    
    .component('lookup', lookup)

    .name
;
