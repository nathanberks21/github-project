import template from './lookup.template.html';

const lookup = {
    template,
    controller: function(GitHub) {
        // Click event handler to call the GitHub getContributors function
        this.lookup = (user, repo) => GitHub.getContributors(user, repo);
    }
};

export default lookup;