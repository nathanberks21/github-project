export default function rxFactory($window) {
    if (!$window.hasOwnProperty('Rx')) {
        throw new Error('Application requires RxJS');
    }
    return $window.Rx;
}
