export default function d3Factory($window) {
    if (!$window.hasOwnProperty('d3')) {
        throw new Error('Application requires D3');
    }
    return $window.d3;
}
