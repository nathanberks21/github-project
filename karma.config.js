const webpack = require('webpack');
const webpackConfig = require('./webpack.config');

module.exports = function(config) {
    config.set({
        frameworks: ['mocha', 'sinon-chai', 'source-map-support'],
        browsers: ['ChromeHeadless'],
        customLaunchers: {
            ChromeHeadless: {
                base: 'Chrome',
                flags: [
                    '--headless',
                    '--disable-gpu',
                    '--remote-debugging-port=9222'
                ]
            }
        },
        client: {
            chai: { includeStack: true }
        },
        files: [
            'node_modules/angular/angular.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'node_modules/rxjs/bundles/Rx.js',
            'node_modules/d3/build/d3.js',
            'src/**/*.test.js'
        ],
        preprocessors: {
            'src/**/*.test.js': ['webpack', 'sourcemap']
        },
        webpack: {
            devtool: 'inline-source-map',
            externals: {
                angular: true
            },
            module: webpackConfig.module,
        },
        webpackMiddleware: {
            noInfo: true
        },
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun: false,
        retryLimit: 5,
        captureTimeout: 2000,
        browserDisconnectTimeout: 10000,
        browserDisconnectTolerance: 5,
        browserNoActivityTimeout: 100000
    });
};