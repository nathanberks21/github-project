const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// External libraries
const libs = [
    { from: './node_modules/angular/angular.js',            to: './lib/' },
    { from: './node_modules/d3/build/d3.js',                to: './lib/' },
    { from: './node_modules/rxjs/bundles/Rx.js',            to: './lib/' },
];

module.exports = {
    entry: './src/app.js',
    devtool: 'inline-source-map',
    externals: {
        d3        : true,
        angular   : true,
        rxjs      : 'Rx',
        'rxjs/Rx' : 'Rx',
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader'].map(loader => ({ loader }))
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    output: {
        filename: 'bundle.js',
        libraryTarget: 'umd',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new CopyWebpackPlugin(libs)
    ]
};

